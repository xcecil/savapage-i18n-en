<?xml version="1.0" encoding="UTF-8"?>
<!--
This file is part of the SavaPage project <https://savapage.org>
Copyright (c) 2020 Datraverse B.V. | Author: Rijk Ravestein 

SPDX-FileCopyrightText: © 2020 Datraverse BV <info@datraverse.com>
SPDX-License-Identifier: AGPL-3.0-or-later

SavaPage is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.
-->
<!DOCTYPE properties SYSTEM "http://java.sun.com/dtd/properties.dtd">
<properties>

    <entry key="xhtml"><![CDATA[
<div xmlns="http://www.w3.org/1999/xhtml" style="padding: 5px;">
<head>
<style>
.sp-table, .sp-table tr, .sp-table th, .sp-table td {
    border: 1px silver solid;
    border-collapse: collapse;
    color: #292929;
    padding: 5px;
}
.sp-table th {
  background-color: #ECECEC;
  font-weight: normal;
  text-align: left;
}
.sp-small {
    font-size: small;
}
.sp-txt-info {
    color: #666666;
}
.sp-txt-valid {
    color: #006600;
}
.sp-txt-warn {
    color: #DC5C05;
}
.sp-txt-error {
    color: red;
}
.sp-txt-right {
    text-align: right;
}
</style>
</head>

<table class="sp-table">
    <tr>
        <td>Member</td><td>$feed_admin.member;format="xml-encode"$</td>
    </tr>
    <tr>
        <td>Membership</td>
        <td>
            $if(feed_admin.membershipWarning)$
                <span class="sp-txt-warn">
            $else$
                 <span>
            $endif$
            $feed_admin.membership;format="xml-encode"$</span>
        </td>
    </tr>
    <tr>
        <td>Days remaining</td>
        <td>
            $if(feed_admin.daysTillExpiryWarning)$
                <span class="sp-txt-warn">
            $else$
                 <span>
            $endif$
            $feed_admin.daysTillExpiry$</span>
        </td>
    </tr>
    <tr>
        <td>Participants</td><td>$feed_admin.participants$</td>
    </tr>
    <tr>
        <td>Users</td><td>$feed_admin.activeUserCount$</td>
    </tr>
    <tr>
        <td>Mode</td><td>$feed_admin.systemMode;format="xml-encode"$</td>
    </tr>
    <tr>
        <td>Uptime</td><td>$feed_admin.uptime;format="xml-encode"$</td>
    </tr>
    <tr>
        <td>SSL expiry</td>
        $if(feed_admin.sslCert)$
            <td>
                $if(feed_admin.sslCert.notAfterError)$
                    <span class="sp-txt-error">
                $elseif(feed_admin.sslCert.notAfterWarning)$
                    <span class="sp-txt-warn">
                $else$
                    <span>
                $endif$
                $feed_admin.sslCert.notAfter;format="date:medium"$</span>
            </td>
        $else$
           <td>-</td>
        $endif$
    </tr>
    <tr>
        <td>Last backup</td>
        $if(feed_admin.daysSinceLastBackup)$
           <td>
                $if(feed_admin.backupWarning)$
                    <span class="sp-txt-warn">
                $else$
                    <span>
                $endif$
                $feed_admin.daysSinceLastBackup$ days
                </span>
           </td>
        $else$
           <td>-</td>
        $endif$
    </tr>
    
    $if(feed_admin.deadlockCount)$
    <tr>
        <td><span class="sp-txt-error">Deadlocks</span></td>
        <td><span class="sp-txt-error">$feed_admin.deadlockCount$</span></td>
    </tr>
    $endif$
    
    <tr>
        <td>Errors</td>
        $if(feed_admin.errorCount)$
           <td><span class="sp-txt-error">$feed_admin.errorCount$</span></td>
        $else$
           <td>-</td>
        $endif$
    </tr>
    <tr>
        <td>Warnings</td>
        $if(feed_admin.warningCount)$
           <td><span class="sp-txt-warn">$feed_admin.warningCount$</span></td>
        $else$
           <td>-</td>
        $endif$
    </tr>

    <tr>
        <td>User Homes</td>
        $if(feed_admin.userHomeStats)$
           <td>$feed_admin.userHomeStats.users$</td>
        $else$
           <td>-</td>
        $endif$
    </tr>
    
    $if(feed_admin.userHomeStats.inboxDocs)$
        <tr>
            <td class="sp-txt-right sp-small">
                Print-in&nbsp;&bull;&nbsp;$feed_admin.userHomeStats.inboxDocs$
            </td>
            <td class="sp-small">$feed_admin.userHomeStats.inboxBytes$</td>
        </tr>
    $endif$
    
    $if(feed_admin.userHomeStats.outboxDocs)$
        <tr>
            <td class="sp-txt-right sp-small">
                Hold Print&nbsp;&bull;&nbsp;$feed_admin.userHomeStats.outboxDocs$
            </td>
            <td class="sp-small">$feed_admin.userHomeStats.outboxBytes$</td>
        </tr>
    $endif$

    $if(feed_admin.userHomeStats.cleanupFiles)$
        <tr>
            <td class="sp-txt-right sp-small">
                $if(feed_admin.userHomeStats.cleaned)$
                    <span class="sp-txt-valid">Cleaned
                $else$
                    <span class="sp-txt-warn">Cleanable
                $endif$
                    &nbsp;&bull;&nbsp;$feed_admin.userHomeStats.cleanupFiles$
                </span> 
            </td>
            <td class="sp-small">
                $if(feed_admin.userHomeStats.cleaned)$
                    <span class="sp-txt-valid">
                $else$
                    <span class="sp-txt-warn">
                $endif$
                    $feed_admin.userHomeStats.cleanupBytes$
                </span>
            </td>
        </tr>
    $endif$

    $if(feed_admin.userHomeStats.letterheadDocs)$
        <tr>
            <td class="sp-txt-right sp-small">
                <span class="sp-txt-info">
                    Letterheads&nbsp;&bull;&nbsp;$feed_admin.userHomeStats.letterheadDocs$
                </span>
            </td>
            <td class="sp-small">
                <span class="sp-txt-info">
                    $feed_admin.userHomeStats.letterheadBytes$
                </span>
            </td>
        </tr>
    $endif$

    $if(feed_admin.userHomeStats.pgpPubRingFiles)$
        <tr>
            <td class="sp-txt-right sp-small">
                <span class="sp-txt-info">
                    PGP Key Ring&nbsp;&bull;&nbsp;$feed_admin.userHomeStats.pgpPubRingFiles$
                </span>
            </td>
            <td class="sp-small">
                <span class="sp-txt-info">
                    $feed_admin.userHomeStats.pgpPubRingBytes$
                </span>
            </td>
        </tr>
    $endif$

    $if(feed_admin.userHomeStats.unknownFiles)$
        <tr>
            <td class="sp-txt-right sp-small">
                <span class="sp-txt-warn">
                    Unknown&nbsp;&bull;&nbsp;$feed_admin.userHomeStats.unknownFiles$
                </span> 
            </td>
            <td class="sp-txt-warn sp-small">
                <span class="sp-txt-warn">
                    $feed_admin.userHomeStats.unknownBytes$
                </span>
            </td>
        </tr>
    $endif$

    $if(feed_admin.userHomeStats.duration)$
        <tr>
            <td class="sp-txt-right sp-small">
                <span class="sp-txt-info">
                    &nbsp;
                </span>
            </td>
            <td class="sp-small">
                <span class="sp-txt-info">
                    $feed_admin.userHomeStats.duration$
                </span>
            </td>
        </tr>
    $endif$

    <tr>
        <td>Tickets</td>
        $if(feed_admin.ticketCount)$
           <td>$feed_admin.ticketCount$</td>
        $else$
           <td>-</td>
        $endif$
    </tr>
</table>
<br/>
<table class="sp-table">
    <tr>
        <th>Documents</th><th>Pages</th>
    </tr>
    <tr>
        <td>Received</td><td align="right">$feed_admin.pagesReceived$</td>
    </tr>
    <tr>
        <td>Printed</td><td align="right">$feed_admin.pagesPrinted$</td>
    </tr>
    <tr>
        <td>Downloaded</td><td align="right">$feed_admin.pagesDownloaded$</td>
    </tr>
</table>

$! +---------------------------------------- +!$
$! |          SNMP Printer status            |!$
$! +---------------------------------------- +!$
$if(feed_admin.printersSnmp)$
<br/> 
<table class="sp-table">
    <tr>
        <th class="sp-small">Queue</th><th class="sp-small">Printer</th>
    </tr>
$feed_admin.printersSnmp:{p|
    <tr>
        <td valign="top" class="sp-small">
        $p.names:{name|$name;format="xml-encode"$<br/>}$
        </td>
        <td valign="top" class="sp-small">
        $if(p.model)$$p.model;format="xml-encode"$
        $else$ &#8208; $endif$
        $if(p.serial)$&nbsp;&bull;&nbsp;$endif$
        $p.serial;format="xml-encode"$
        <br/>$p.date;format="full"$
        $p.alerts:{alert|<br/><span class="sp-txt-warn">$alert$</span>}$
        
        $if(p.markerNames)$<br/>$endif$
        $p.markerNames,p.markerPercs:{ n, p |&nbsp;&bull;&nbsp;$n$&nbsp;$p$%}$
        
        </td>
    </tr>
}$
</table>
$endif$

$! +---------------------------------------- +!$
<p style="color: gray;" class="sp-small">$app.nameVersionBuild;format="xml-encode"$</p>
</div>
]]></entry>

</properties>
