<?xml version="1.0" encoding="UTF-8"?>
<!--
This file is part of the SavaPage project <https://savapage.org>
Copyright (c) 2020 Datraverse B.V. | Author: Rijk Ravestein 

SPDX-FileCopyrightText: © 2020 Datraverse BV <info@datraverse.com>
SPDX-License-Identifier: AGPL-3.0-or-later

SavaPage is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.
-->
<!DOCTYPE properties SYSTEM "http://java.sun.com/dtd/properties.dtd">
<properties>

    <!-- WebApp User/Admin Client -->

    <entry key="msg-exception">An error occurred, please contact your system
        administrator.</entry>

    <entry key="msg-invalid-request">Request "{0}" is invalid.</entry>

    <entry key="msg-ecoprint-pending">Eco Print is being prepared. Please wait a moment
        and try again.</entry>
    <entry key="msg-select-single-pdf-filter">Selection of multiple PDF filters is not supported:
        please select one filter.</entry>

    <entry key="exc-letterhead-not-found">Letterhead not found. Please refresh
        your letterhead
        list.</entry>
    <entry key="exc-letterhead-not-found-login">Letterhead not found. Please login again.</entry>

    <entry key="msg-need-admin-rights">You need to have administrator rights for this
        function.</entry>
    <entry key="msg-user-not-authorized">Your session expired or is not available. Please
        login again.</entry>

    <entry key="msg-mail-sent">Mail sent to {0}</entry>
    <entry key="msg-mail-max-file-size">The PDF is {0} and is not send because the max
        attachment is {1}. Please download the PDF and use your own
        email client to send it.</entry>

    <entry key="msg-page-delete-one">{0} page deleted.</entry>
    <entry key="msg-page-delete-multiple">{0} pages deleted.</entry>

    <entry key="msg-config-props-applied">Configuration options are applied.</entry>

    <entry key="msg-printer-out-of-date">Printer information is out-of-date. Please refresh
        the printer list.</entry>
    <entry key="msg-printer-no-media-sources-defined">No media sources defined for printer {0}.
        Please
        contact the application administrator.</entry>

    <entry key="msg-user-pdf-out-disabled">You are not allowed to export to PDF.</entry>

    <entry key="msg-pdf-export-drm-error">DRM-restricted SafePages: PDF export is not
        permitted.</entry>

    <entry key="msg-clear-range-syntax-error">Syntax of page range "{0}" is invalid.</entry>

    <entry key="msg-print-auth-cancel-ok">Print job successfully cancelled.</entry>
    <entry key="msg-print-auth-cancel-not-found">Print job to cancel not found.</entry>

    <entry key="msg-print-fast-renew-ok">Fast Print Closing Time extended till {0}</entry>

    <entry key="msg-page-move-one">{0} page moved.</entry>
    <entry key="msg-page-move-multiple">{0} pages moved.</entry>

    <entry key="msg-job-deleted">Job successfully deleted.</entry>
    <entry key="msg-job-edited">Job successfully edited.</entry>
    <entry key="msg-letterhead-edited">Letterhead successfully edited.</entry>

    <entry key="msg-tech-error">{0}</entry>

    <entry key="msg-user-sync-busy">Synchronizing users...</entry>
    <entry key="msg-password-reset-ok">Password has been reset.</entry>
    <entry key="msg-password-reset-not-allowed">You are not allowed to reset password for user
        "{0}".</entry>
    <entry key="msg-password-length-error">Password should be at least {0} characters long.</entry>

    <entry key="msg-pin-reset-ok">PIN has been reset.</entry>
    <entry key="msg-user-pin-not-numeric">PIN must have digits only.</entry>
    <entry key="msg-user-pin-length-error-min">PIN should be at least {0} digits long.</entry>
    <entry key="msg-user-pin-length-error-min-max">PIN should be between {0} and {1} digits long.</entry>
    <entry key="msg-user-pin-length-error">PIN should be {0} digits long.</entry>

    <entry key="msg-device-printer-group-not-found">Proxy Printer Group "{0}" is not found.</entry>

    <entry key="msg-printer-saved-ok">Proxy Printer successfully saved.</entry>
    <entry key="msg-printer-not-found">Proxy Printer "{0}" is not found.</entry>

    <entry key="msg-printer-renamed-ok">Proxy Printer "{0}" successfully renamed to "{1}".</entry>
    <entry key="msg-printer-renamed-delete-ok">Proxy Printer "{0}" successfully renamed to "{1}"
        (existing Proxy Printer is removed).</entry>
    <entry key="msg-printer-rename-duplicate">Proxy Printer "{0}" cannot be renamed to "{1}"
        (Proxy Printer with same name already present).</entry>
    <entry key="msg-printer-rename-new-name-missing">New printer name is missing.</entry>

    <entry key="msg-user-not-found">User is not found.</entry>

    <entry key="msg-pagometer-reset-ok">Pagometers successfully reset.</entry>

    <entry key="msg-card-unregistered">Card is not registered.</entry>

    <!-- ======================================== -->
    <entry key="msg-imap-test-passed">Passed IMAP test: ({0}) Inbox and ({1}) Trash
        messages.</entry>
    <entry key="msg-imap-test-failed">Failed IMAP test: {0}</entry>
    <entry key="msg-imap-stopped">IMAP monitoring succesfully stopped.</entry>
    <entry key="msg-imap-started">IMAP monitoring succesfully started.</entry>
    <entry key="msg-imap-stopped-already">IMAP monitoring already stopped.</entry>
    <entry key="msg-imap-started-already">IMAP monitoring already started.</entry>

    <entry key="msg-papercut-test-passed">Passed PaperCut test: {0}.</entry>
    <entry key="msg-papercut-test-failed">Failed PaperCut test: {0}</entry>

    <entry key="msg-payment-gateway-online">{0} is set Online.</entry>
    <entry key="msg-payment-gateway-offline">{0} is set Offline.</entry>
    <entry key="msg-payment-gateway-not-found">Payment Gateway not found.</entry>
    <entry key="msg-payment-method-not-possible">{0} payments are currently not possible.</entry>

    <entry key="msg-voucher-batch-created-ok">There are {0} vouchers created in batch {1}.</entry>

    <entry key="msg-voucher-batch-deleted-zero">No vouchers to delete in batch {0}.</entry>
    <entry key="msg-voucher-batch-deleted-one">1 voucher deleted in batch {0}.</entry>
    <entry key="msg-voucher-batch-deleted-many">{0} vouchers deleted in batch {1}.</entry>

    <entry key="msg-voucher-batch-expired-zero">No vouchers to expire in batch {0}.</entry>
    <entry key="msg-voucher-batch-expired-one">Expired 1 voucher in batch {0}.</entry>
    <entry key="msg-voucher-batch-expired-many">Expired {0} vouchers in batch {1}.</entry>

    <entry key="msg-voucher-deleted-expired-zero">No expired vouchers to delete.</entry>
    <entry key="msg-voucher-deleted-expired-one">1 expired voucher deleted.</entry>
    <entry key="msg-voucher-deleted-expired-many">{0} expired vouchers deleted.</entry>

    <entry key="msg-voucher-redeem-ok">The value of the voucher is successfully added to
        your balance.</entry>
    <entry key="msg-voucher-redeem-invalid">User "{0}" tried to redeem invalid voucher "{1}".</entry>
    <entry key="msg-voucher-redeem-void">Please enter a valid voucher number.</entry>

    <entry key="msg-deposit-funds-ok">Amount successfully added to user account.</entry>
    <entry key="msg-deposit-funds-receipt-email-ok">Amount successfully added to user account. Receipt
        sent to {0}</entry>

    <entry key="msg-deposit-email-subject">Receipt {0}</entry>
    <entry key="msg-deposit-email-body">Please find the deposit receipt attached as PDF
        file.</entry>

    <entry key="msg-pos-receipt-sendmail-ok">Receipt successfully sent to {0}</entry>

    <entry key="msg-amount-invalid">Amount is invalid.</entry>
    <entry key="msg-amount-must-be positive">Amount must be greater that zero.</entry>

    <entry key="msg-payment-gateway-comment">{0} credit for {1}.</entry>

</properties>
